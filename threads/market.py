from PyQt5.QtCore import *
import okex.futures_api as future
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import copy
import time
import json


class Market(QThread):
    message = pyqtSignal(str)
    api_key = ""
    seceret_key = ""
    ticker = ""
    resetTime = 0
    _please_stop = True
    amount = 1
    preOrderIdList = []
    orderIdList = []
    closeOrderIdList = []
    spread = 1
    maxAmount = 100
    range = 1

    def run(self):
        while not self._please_stop:
            last = self.futureAPI.get_specific_ticker_last(self.ticker)
            if last is not None:
                last = float(last)
                self.cancelCloseOrder()
                time.sleep(0.1)
                self.checkPosition(last)
                self.createOrder(last)

            time.sleep(self.resetTime)

    def createOrder(self, last):
        bidOrders = []
        askOrders = []
        for i in range(1, self.range+1):
            bidPrice = str(round(last - (i * self.ticSize * self.spread), 3))
            bidOrder = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "1", "price": bidPrice, "size": self.amount, "match_price": "0", "order_type": "1"}
            bidOrders.append(bidOrder)
            askPrice = str(round(last + (i * self.ticSize * self.spread), 3))
            askOrder = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "2", "price": askPrice,
                     "size": self.amount, "match_price": "0", "order_type": "1"}
            askOrders.append(askOrder)
        askOrderData = json.dumps(askOrders)
        bidOrderData = json.dumps(bidOrders)
        try:
            self.preOrderIdList = copy.deepcopy(self.orderIdList)
            self.orderIdList.clear()
            bidResult = self.futureAPI.take_orders(self.ticker, orders_data=bidOrderData, leverage=20)
            if bidResult[0]['result']:
                for data in bidResult[0]['order_info']:
                    self.orderIdList.append(data['order_id'])
            askResult = self.futureAPI.take_orders(self.ticker, orders_data=askOrderData, leverage=20)
            if askResult[0]['result']:
                for data in askResult[0]['order_info']:
                    self.orderIdList.append(data['order_id'])

            self.cancelPreOrder()

        except (ConnectionError, Timeout, TooManyRedirects) as e:
            pass

    def checkPosition(self, last):
        try:
            result = self.futureAPI.get_specific_position(self.ticker)
            if result is not None and result['result']:
                dic = result['holding'][0]
                longAmount = int(dic['long_avail_qty'])
                if longAmount > 0:
                    askOrders = []
                    i = 0
                    while longAmount > 0 :
                        i = i + 1
                        longAmount = longAmount - self.amount
                        if longAmount - self.amount < 0:
                            orderAmount = longAmount
                        else:
                            orderAmount = self.amount
                        askPrice = str(round(last + (i * self.ticSize * self.spread), 3))
                        askOrder = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "3", "price": askPrice,
                                    "size": orderAmount, "match_price": "0", "order_type": "1"}
                        askOrders.append(askOrder)

                    self.placeMultiOrder(askOrders)

                shortAmount = int(dic['short_avail_qty'])
                if shortAmount > 0:
                    bidOrders = []
                    for i in range(0, shortAmount):
                        i = i + 1
                        shortAmount = shortAmount - self.amount
                        if shortAmount - self.amount < 0:
                            orderAmount = shortAmount
                        else:
                            orderAmount = self.amount
                        bidPrice = str(round(last - (i * self.ticSize * self.spread), 3))
                        bidOrder = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "4", "price": bidPrice,
                                    "size": orderAmount, "match_price": "0", "order_type": "1"}
                        bidOrders.append(bidOrder)
                    self.placeMultiOrder(bidOrders)

        except (ConnectionError, Timeout, TooManyRedirects) as e:
            pass

    def placeMultiOrder(self, orderList):
        listLen = len(orderList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 10) > 0:
                tempList = orderList[count * 10: (count+1) * 10]
                count = count + 1
                orderData = json.dumps(tempList)
                result = self.futureAPI.take_orders(self.ticker, orders_data=orderData, leverage=20)
                if result is not None and result[0]['result']:
                    for data in result[0]['order_info']:
                        self.closeOrderIdList.append(data['order_id'])

    def cancelCloseOrder(self):
        listLen = len(self.closeOrderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.closeOrderIdList[count*20:(count+1)*20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.closeOrderIdList.clear()

    def cancelPreOrder(self):
        listLen = len(self.preOrderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.preOrderIdList[count*20:(count+1)*20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.preOrderIdList.clear()

    def cancelOrder(self):
        listLen = len(self.orderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.orderIdList[count*20:(count+1)*20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.orderIdList.clear()

    def startThread(self, api_key, seceret_key, passphrase):
        self.api_key = api_key
        self.seceret_key = seceret_key
        self.passphrase = passphrase
        self.futureAPI = future.FutureAPI(api_key, seceret_key, passphrase, True)
        self._please_stop = False
        self.start()

    def stopThread(self):
        self._please_stop = True

    def setTicker(self, ticker):
        self.ticker = ticker

    def setTicSize(self, ticSize):
        self.ticSize = float(ticSize)

    def setResetTime(self, resetTime):
        self.resetTime = int(resetTime)

    def setAmount(self, amount):
        self.amount = int(amount)

    def setMaxAmount(self, max):
        self.maxAmount = int(max)

    def setSpread(self, spread):
        self.spread = int(spread)

    def setRange(self, start):
        self.range = int(start)
