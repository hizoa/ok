import time
from PyQt5.QtCore import *
import okex.futures_api as future
import json
import datetime
import pandas as pd
from pandas import DataFrame as df
import matplotlib.pyplot as plt
import numpy as np
from common import util
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects


class Band(QThread):
    message = pyqtSignal(str)
    closeLog = pyqtSignal(dict)
    _please_stop = True
    ticker = ""
    resetTime = 10
    amount = 1
    ticSize = 0
    spread = 3000
    restTime = 0
    cancelTime = 600
    cutPrice = 10000
    isCreateOrder = False;
    preOrderIdList = []
    orderIdList = []
    closeOrderIdList = []
    lastPrice = ""
    lastOrderTime = datetime.datetime.now()
    api_key = ""
    seceret_key = ""
    passphrase = ""
    isLongOrdered = False
    isShortOrdered = False
    clearLong = False
    clearShort = False
    currentStatus = 0   #none:0 , long:1 , short:2 , longClose:3, shortClose:4
    standby = 0 #long : 0, short: 1
    LONG_STANDBY = 1
    SHORT_STANDBY = 2


    def run(self):
        while not self._please_stop:
            self.futureAPI = future.FutureAPI(self.api_key, self.seceret_key, self.passphrase, True)
            try:
                last = self.futureAPI.get_specific_ticker_last(self.ticker)
                if last is not None and util.isNumber(last) and float(last) > 0:
                    self.lastPrice = float(last)
                oscillator = self.getOscillator()
                if self.checkPosition(oscillator):
                    self.createOrder(oscillator)
            except (ConnectionError, Timeout, TooManyRedirects) as e:
                pass
            time.sleep(self.resetTime)

    def createOrder(self, oscil):
        if self.lastPrice is not None and util.isNumber(self.lastPrice) and float(self.lastPrice) > 0:
            last = float(self.lastPrice)
            try:
                orders = []
                if oscil == 1 and self.standby == self.LONG_STANDBY and not self.isLongOrdered:
                    price = str(last - (self.ticSize * self.spread))
                    order = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "1", "price": price,
                             "size": self.amount, "match_price": "0"}
                    orders.append(order)
                    self.standby = self.SHORT_STANDBY
                    orders_data = json.dumps(orders)
                    result = self.futureAPI.take_orders(self.ticker, orders_data=orders_data, leverage=20)
                    if result[0]['result']:
                        self.isLongOrdered = True
                        self.lastOrderTime = datetime.datetime.now()
                        self.orderIdList.clear()
                        for data in result[0]['order_info']:
                            self.orderIdList.append(data['order_id'])
                        self.message.emit(str(datetime.datetime.now()) + "롱 주문완료 " + str(price))
                elif oscil == 2 and self.standby == self.SHORT_STANDBY and not self.isShortOrdered:
                    price = str(last + (self.ticSize * self.spread))
                    order = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "2", "price": price,
                             "size": self.amount, "match_price": "0"}
                    orders.append(order)
                    self.standby = self.LONG_STANDBY
                    orders_data = json.dumps(orders)
                    result = self.futureAPI.take_orders(self.ticker, orders_data=orders_data, leverage=20)
                    if result[0]['result']:
                        self.isShortOrdered = True
                        self.lastOrderTime = datetime.datetime.now()
                        self.orderIdList.clear()
                        for data in result[0]['order_info']:
                            self.orderIdList.append(data['order_id'])
                        self.message.emit(str(datetime.datetime.now()) + "숏 주문완료 " + str(price))
            except (ConnectionError, Timeout, TooManyRedirects) as e:
                pass

    def checkPosition(self, oscillator):
        #1. 물량을 가지고 있으면 청산전략
        #2. 물량을 가지고 있지 않으면 진입전략
        #3. 시간이 지나면 종료전략
        try:
            result = self.futureAPI.get_specific_position(self.ticker)
            if result is not None and result['result'] and result['holding'].__len__() > 0:
                dic = result['holding'][0]
                if float(dic['long_qty']) > 0:
                    availQty = float(dic['long_avail_qty'])
                    if float(self.lastPrice) < float(dic['long_avg_cost']) - (self.cutPrice * self.ticSize):
                        self.clearLong = True
                        self.message.emit("롱 손절 " + str(self.lastPrice))
                    if availQty > 0:
                        if self.clearLong:
                            result = self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687", self.ticker, "3", 0, dic['long_avail_qty'], 1, 20)
                            self.cancelOrder()
                            self.standby = self.SHORT_STANDBY
                            self.isLongOrdered = False
                            self.message.emit(str(datetime.datetime.now()) + "롱 전부 청산 이제 숏 대기중")
                    return False

                elif float(dic['short_qty']) > 0:
                    availQty = float(dic['short_avail_qty'])
                    if float(self.lastPrice) > float(dic['short_avg_cost']) + (self.cutPrice * self.ticSize):
                        self.clearLong = True
                        self.message.emit("숏 손절 " + str(self.lastPrice))
                    if availQty > 0:
                        if self.clearShort:
                            result = self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687", self.ticker, "4", 0, dic['short_qty'], 1, 20)
                            self.cancelOrder()
                            self.standby = self.LONG_STANDBY
                            self.isShortOrdered = False
                            self.message.emit(str(datetime.datetime.now()) + "숏 전부 청산 이제 롱 대기중")
                    return False

                else:
                    #오더가 걸려있다.
                    res = self.futureAPI.get_order_list("0", self.ticker, "", "", "")
                    if res is not None and res[1].__len__() > 0:
                        pastTime = (datetime.datetime.now() - self.lastOrderTime).seconds
                        if pastTime > self.cancelTime:
                            self.cancelAllOrder(res[0])
                            if self.standby == self.LONG_STANDBY:
                                self.message.emit(str(datetime.datetime.now()) + "주문전부 취소 이제 롱 대기중")
                            else:
                                self.message.emit(str(datetime.datetime.now()) + "주문전부 취소 이제 숏 대기중")
                    else:
                        self.isShortOrdered = False
                        self.isLongOrdered = False
        except (ConnectionError, Timeout, TooManyRedirects) as e:
            pass

        return True

    def cancelAllOrder(self, dic):
        if dic is None:
            dic = self.futureAPI.get_order_list("0", self.ticker, "", "", "")[1]
        orderInfo = dic['order_info']
        self.orderIdList.clear()
        for item in orderInfo:
            self.orderIdList.append(item['order_id'])
        self.cancelOrder()
        self.isLongOrdered = False
        self.isShortOrdered = False

    def cancelOrder(self):
        listLen = len(self.orderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.orderIdList[count * 20:(count + 1) * 20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.orderIdList.clear()

    def startThread(self, api_key, seceret_key, passphrase):
        self.api_key = api_key
        self.seceret_key = seceret_key
        self.passphrase = passphrase
        self._please_stop = False
        self.start()

    def stopThread(self):
        self._please_stop = True
        self.cancelOrder()

    def setTicker(self, ticker):
        self.ticker = ticker

    def setTicSize(self, ticSize):
        self.ticSize = float(ticSize)

    def setResetTime(self, resetTime):
        self.resetTime = int(resetTime)

    def setOrderAmount(self, amount):
        self.amount = int(amount)

    def setSpread(self, spread):
        self.spread = int(spread)

    def setCancel(self, cancel):
        self.cancelTime = int(cancel)

    def setCutPrice(self, price):
        self.cutPrice = int(price)

    # https://excelsior-cjh.tistory.com/111
    def getOscillator(self, period=300):
        try:
            candleList = self.futureAPI.get_kline(self.ticker, period, '', "")[0]
            length = 8
            forV2 = 5
            lvls = 0.8
            fr = True
            src = []
            hlc = []

            m_Df = pd.DataFrame(candleList)

            #첫번째 캔들은 무시한다
            for i in range(0, 100 + length + forV2):
                src.insert(i, round((float(candleList[i + 1][2]) + float(candleList[i + 1][3])) / 2, 3))
                hlc.insert(i, round((float(candleList[i + 1][2]) + float(candleList[i + 1][3]) + float(candleList[i + 1][4])) / 3, 3))

            tempCg = []
            for cnt in range(0, length + forV2):
                nm = []
                dm = []
                nm.insert(0, 0)
                dm.insert(0, 0)
                for i in range(0, 100):
                    nm.insert(i + 1, nm[i] + (1 + i) * src[i + cnt] if i + 1 <= length else nm[i])
                    dm.insert(i + 1, (dm[i] + src[i + cnt] if i + 1 <= length else dm[i]))
                tempCg.insert(cnt, -nm[100] / dm[100] + (length + 1) / 2.0 if dm[100] != 0 else 0)

            cg = pd.Series(tempCg)

            maxCg = []
            minCg = []
            for cnt in range(0, forV2):
                maxCg.insert(cnt, cg[0 + cnt:length + cnt].max())
                minCg.insert(cnt, cg[0 + cnt:length + cnt].min())

            v1 = []
            for cnt in range(0, forV2):
                v1.insert(cnt, (cg[cnt] - minCg[cnt]) / (maxCg[cnt] - minCg[cnt]) if maxCg[cnt] != minCg[cnt] else 0)
            v2_ = ((4 * v1[0]) + (3 * v1[1]) + (2 * v1[2]) + (1 * v1[3])) / 10.0
            v2 = 2 * (v2_ - 0.5)
            preV2_ = ((4 * v1[1]) + (3 * v1[2]) + (2 * v1[3]) + (1 * v1[4])) / 10.0
            preV2 = 2 * (preV2_ - 0.5)
            green = 0.96 * (preV2 + 0.02)

            # total = 0
            # for cnt in range(5, 15):
            #     total = total + hlc[cnt]
            # total = total / 10

#스토캐스틱
            # daysHigh = df.get(2).rolling(14).max()
            # dayLow = df.get(3).rolling(14).min()
            # daysHigh.fillna(0)
            # dayLow.fillna(0)
            # temp_k = df.get(4).astype(float)
            # fastK = ((temp_k - dayLow) / (daysHigh - dayLow)) * 100
            # slowK = fastK.ewm(span=3).mean()
            # slowD = slowK.ewm(span=3).mean()

            #지수가중평균
            #ap = (df.get(2).astype(float) + df.get(3).astype(float) + df.get(4).astype(float)) / 3
            ap = pd.Series(hlc)
            esa = ap.ewm(span=10, min_periods=10).mean()
            #esa2 = esa.dropna().reset_index(drop=True)
            #ap2 = ap[0:esa2.size]
            d = abs(ap - esa).ewm(span=10, min_periods=10).mean()
            #d2 = d.dropna().reset_index(drop=True)
            ci = (ap - esa) / (0.015 * d)
            tci = ci.ewm(span=21, min_periods=21).mean()
            #tci2 = tci.dropna().reset_index(drop=True)
            wt1 = tci
            wt2 = pd.Series.rolling(wt1, 4).mean()
            checkEma = wt2[41]

            print(str(datetime.datetime.now()) + "triger=" + str(green) + "osc=" + str(v2) + "osc2=" + str(preV2)  + " check=" + str(checkEma))
            if v2 <= -0.95 and self.standby != self.SHORT_STANDBY:
                if checkEma > -10:
                    self.standby = self.SHORT_STANDBY
                    self.message.emit(
                        str(datetime.datetime.now()) + "체크조건 맞지 않아 롱패스 숏대기 osc=" + str(v2) + " check=" + str(
                            checkEma))
                else:
                    self.currentStatus = 1
                    self.standby = self.LONG_STANDBY
                    self.clearShort = True
                    self.clearLong = False
                    self.message.emit(str(datetime.datetime.now()) + " 롱대기 osc=" + str(v2) + " check=" + str(checkEma))
            elif v2 >= 0.95 and self.standby != self.LONG_STANDBY:
                if checkEma < 10:
                    self.standby = self.LONG_STANDBY
                    self.message.emit(
                        str(datetime.datetime.now()) + "체크조건 맞지 않아 숏패스 롱대기 osc=" + str(v2) + " check=" + str(
                            checkEma))
                else:
                    self.currentStatus = 2
                    self.standby = self.SHORT_STANDBY
                    self.clearShort = False
                    self.clearLong = True
                    self.message.emit(str(datetime.datetime.now()) + "숏대기 osc=" + str(v2) + " check=" + str(checkEma))
            elif v2 > 0.8:
                self.currentStatus = 3
                self.clearShort = False
                self.clearLong = True
            elif v2 < -0.8:
                self.currentStatus = 4
                self.clearShort = True
                self.clearLong = False
            else:
                self.standby = 0
                self.currentStatus = 0

            return self.currentStatus
        except (ConnectionError, TooManyRedirects, Timeout) as e:
            print(e)







