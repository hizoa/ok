import time
from PyQt5.QtCore import *
import okex.futures_api as future
import json
import datetime
import copy
import pandas
import numpy

from common import util
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects


class Worker(QThread):
    message = pyqtSignal(str)
    closeLog = pyqtSignal(dict)
    _please_stop = True
    ticker = ""
    resetTime = ""
    amount = ""
    bidStart = 0
    bidEnd = 0
    askStart = 0
    askEnd = 0
    cutTime = 0
    cutPrice = 0
    clearTime = 0
    bidSpread = 0
    askSpread = 0
    ticSize = 0
    restTime = 0
    close1 = 0
    close2 = 0
    preOrderIdList = []
    orderIdList = []
    closeOrderIdList = []
    lastPrice = ""
    lastOrderTime = datetime.datetime.now()
    api_key = ""
    seceret_key = ""
    passphrase = ""
    reFreshPrice = False

    def run(self):
        while not self._please_stop:
            self.futureAPI = future.FutureAPI(self.api_key, self.seceret_key, self.passphrase, True)
            if self.checkPosition():
                try:
                    self.cancelOrder()
                    self.lastPrice = self.futureAPI.get_specific_ticker_last(self.ticker)
                    self.createOrder()
                except (ConnectionError, Timeout, TooManyRedirects) as e:
                    pass
            time.sleep(self.resetTime)

    def createOrder(self):
        if self.lastPrice is not None and util.isNumber(self.lastPrice) and float(self.lastPrice) > 0:
            last = float(self.lastPrice)
            try:
                line = self.getMovingAvg(5)  # 5분이평보다 높으면 패스
                if last > line:
                    print("1분봉 5이평보다 위")
                    return
                orders = []
                for i in range(self.bidStart, self.bidEnd):
                    price = str(last - (i * self.ticSize * self.bidSpread))
                    order = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "1", "price": price, "size": self.amount, "match_price": "0"}
                    orders.append(order)
                orders_data = json.dumps(orders)
                result = self.futureAPI.take_orders(self.ticker, orders_data=orders_data, leverage=20)
                if result[0]['result']:
                    #self.preOrderIdList = copy.deepcopy(self.orderIdList)
                    self.orderIdList.clear()
                    for data in result[0]['order_info']:
                        self.orderIdList.append(data['order_id'])
                    #self.cancelPreOrder()
            except (ConnectionError, Timeout, TooManyRedirects) as e:
                pass

    def getMovingAvg(self, line):
        candleList = self.futureAPI.get_kline(self.ticker, 60, '', "")[0]
        sumCandle = 0
        for i in range(0, line):
            sumCandle = sumCandle + float(candleList[i][4])
        return round(sumCandle/line, 3)

    def checkPosition(self):
        try:
            result = self.futureAPI.get_specific_position(self.ticker)
            if result is not None and result['result'] and result['holding'].__len__() > 0:
                dic = result['holding'][0]
                if float(dic['long_qty']) > 0:

                    avgCost = float(dic['long_avg_cost'])
                    availQty = float(dic['long_avail_qty'])
                    if availQty > 0 and util.isNumber(self.lastPrice):
                        # result = self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687", self.ticker, "3",
                        #                                    price, dic['long_avail_qty'], 0, 20)
                        # if result['result']:
                        #     self.closeOrderIdList.append(result['order_id'])
                        self.closeOrder(availQty, avgCost)
                        self.cancelPreOrder()
                        self.cancelOrder()
                        self.lastOrderTime = datetime.datetime.now()

                    # 청산전략
                    pastTime = (datetime.datetime.now() - self.lastOrderTime).seconds
                    if pastTime > self.cutTime:
                        # 10초가 지났는데도 지금가격보다 낮다면?
                        avgCost = float(dic['long_avg_cost']) - self.cutPrice
                        curCost = float(self.futureAPI.get_specific_ticker(self.ticker)['last'])
                        if curCost is not None:
                            if avgCost > curCost or pastTime > self.clearTime:
                                self.cancelPreOrder()
                                self.cancelOrder()
                                self.cancelCloseOrder()
                                time.sleep(0.5)
                                result = self.futureAPI.get_specific_position(self.ticker)
                                if result['result']:
                                    dic = result['holding'][0]
                                    if float(dic['long_avail_qty']) > 0:
                                        result = self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687",
                                                                           self.ticker, "3",
                                                                           self.lastPrice, dic['long_avail_qty'], 1, 20)
                                        # 떡락할수 있기 때문에 시장가 이후는 여유
                                        time.sleep(self.restTime)
                            # elif pastTime > 20:
                            #     self.reCloseOrder()
                        else:
                            print("get_specific_ticker none")
                    return False
        except (ConnectionError, Timeout, TooManyRedirects) as e:
            pass

        return True

    def reCloseOrder(self):
        self.cancelPreOrder()
        self.cancelOrder()
        self.cancelCloseOrder()
        self.reFreshPrice = True

    def closeOrder(self, availQty, avaPrice):
        if self.reFreshPrice:
            self.reFreshPrice = False
            self.lastPrice = self.futureAPI.get_specific_ticker(self.ticker)['last']
            price = float(self.lastPrice) + 0.003
        else:
            price = max(float(self.lastPrice) + (self.ticSize * self.close2), float(avaPrice) + (self.ticSize * self.close1))

        self.closeLog.emit({"last": self.lastPrice, "avg": float(avaPrice), "price": price})
        orders = []
        count = 0
        while availQty > 0:
            orderSize = availQty
            if availQty > self.amount:
                orderSize = self.amount
            order = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "3",
                     "price": float(price) + (self.ticSize * count * self.bidSpread),
                     "size": int(orderSize), "match_price": "0"}
            orders.append(order)
            availQty = availQty - self.amount
            count = count + 1

        listLen = len(orders)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 10) > 0:
                tempList = orders[count*10:(count+1)*10]
                count = count + 1
                orders_data = json.dumps(tempList)
                result = self.futureAPI.take_orders(self.ticker, orders_data=orders_data, leverage=20)
                if result[0]['result']:
                    for data in result[0]['order_info']:
                        self.closeOrderIdList.append(data['order_id'])

    def cancelCloseOrder(self):
        listLen = len(self.closeOrderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.closeOrderIdList[count*20:(count+1)*20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.closeOrderIdList.clear()

    def cancelPreOrder(self):
        listLen = len(self.preOrderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.preOrderIdList[count*20:(count+1)*20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.preOrderIdList.clear()

    def cancelOrder(self):
        listLen = len(self.orderIdList)
        if listLen > 0:
            tempList = []
            count = 0
            while listLen - (count * 20) > 0:
                tempList = self.orderIdList[count*20:(count+1)*20]
                count = count + 1
                result = self.futureAPI.revoke_orders(self.ticker, tempList)
            self.orderIdList.clear()

    def startThread(self, api_key, seceret_key, passphrase):
        self.api_key = api_key
        self.seceret_key = seceret_key
        self.passphrase = passphrase
        self._please_stop = False
        self.start()

        # result = futureAPI.get_position()
        # result = futureAPI.get_coin_account('btc')
        # result = futureAPI.get_leverage('btc')
        # result = futureAPI.set_leverage(symbol='BTC', instrument_id='BCH-USD-181026', direction=1, leverage=10)
        # result = futureAPI.take_order()

        # take orders
        # orders = []
        # order1 = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "2", "price": "1800.0", "size": "1", "match_price": "0"}
        # order2 = {"client_oid": "f379a96206fa4b778e1554c6dc969687", "type": "2", "price": "1800.0", "size": "1", "match_price": "0"}
        # orders.append(order1)
        # orders.append(order2)
        # orders_data = json.dumps(orders)
        # result = futureAPI.take_orders('BCH-USD-181019', orders_data=orders_data, leverage=10)

        # result = futureAPI.get_ledger('btc')
        # result = futureAPI.get_products()
        # result = futureAPI.get_depth('BTC-USD-181019', 1)
        # result = futureAPI.get_ticker()
        # result = futureAPI.get_specific_ticker('ETC-USD-181026')
        # result = futureAPI.get_specific_ticker('ETC-USD-181026')
        # result = futureAPI.get_trades('ETC-USD-181026', 1, 3, 10)
        # result = futureAPI.get_kline('ETC-USD-181026','2018-10-14T03:48:04.081Z', '2018-10-15T03:48:04.081Z')
        # result = futureAPI.get_index('EOS-USD-181019')
        # result = futureAPI.get_products()
        # result = futureAPI.take_order("ccbce5bb7f7344288f32585cd3adf357", 'BTC-USD-190712','1','12000','1','0','20')
        # result = futureAPI.take_order("ccbce5bb7f7344288f32585cd3adf351",'BCH-USD-181019',2,10000.1,1,0,10)
        # result = futureAPI.get_trades('BCH-USD-181019')
        # result = futureAPI.get_rate()
        # result = futureAPI.get_estimated_price('BTC-USD-181019')
        # result = futureAPI.get_holds('BTC-USD-181019')
        # result = futureAPI.get_limit('BTC-USD-181019')
        # result = futureAPI.get_liquidation('BTC-USD-181019', 0)
        # result = futureAPI.get_holds_amount('BCH-USD-181019')
        # result = futureAPI.get_mark_price('BCH-USD-181019')

    def stopThread(self):
        self._please_stop = True
        self.cancelPreOrder()
        self.cancelOrder()
        self.cancelCloseOrder()


    def setTicker(self, ticker):
        self.ticker = ticker

    def setTicSize(self, ticSize):
        self.ticSize = float(ticSize)

    def setResetTime(self, resetTime):
        self.resetTime = int(resetTime)

    def setOrderAmount(self, amount):
        self.amount = int(amount)

    def setAskStart(self, askStart):
        self.askStart = int(askStart)

    def setAskEnd(self, askEnd):
        self.askEnd = int(askEnd)

    def setBidStart(self, bidStart):
        self.bidStart = int(bidStart)

    def setBidEnd(self, bidEnd):
        self.bidEnd = int(bidEnd)

    def setBidSpread(self, spread):
        self.bidSpread = int(spread)

    def setAskSpread(self, spread):
        self.askSpread = int(spread)

    def setCutTime(self, cutTime):
        self.cutTime = float(cutTime)

    def setCutPrice(self, cutPrice):
        self.cutPrice = float(cutPrice)

    def setClearTime(self, clearTime):
        self.clearTime = float(clearTime)

    def setRestTime(self, restTime):
        self.restTime = int(restTime)

    def setClose1(self, close1):
        self.close1 = close1

    def setClose2(self, close2):
        self.close2 = close2







