from PyQt5.QtCore import *
import okex.futures_api as future
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import datetime
from bitmex import bitmex_websocket
import logging
import websocket


class Pududu(QThread):
    message = pyqtSignal(str)
    api_key = ""
    seceret_key = ""
    ticker = ""
    upTime = 0
    upTic = 0
    upAmount = 0
    downTime = 0
    downTic = 0
    downAmount = 0
    lastPrice = 0
    ticSize = 0.0
    checkUp = False
    checkDown = False
    _please_stop = True
    sellWaiting = False
    buyWaiting = False
    openOrder = False
    mainDic = {}

    def run(self):
        logger = self.setup_logger()
        ws = bitmex_websocket.BitMEXWebsocket(endpoint="https://testnet.bitmex.com/api/v1", symbol="XBTUSD",
                                              api_key=None,
                                              api_secret=None)
        #logger.info("Instrument data: %s" % ws.get_instrument())
        while not self._please_stop:
            while ws.ws.sock.connected:
                #logger.info("Ticker: %s" % ws.get_ticker())
                #if ws.api_key:
#                    logger.info("Funds: %s" % ws.funds())
#                logger.info("Market Depth: %s" % ws.market_depth())
                logger.info("Recent Trades: %s\n\n" % ws.recent_trades())

            last = self.futureAPI.get_specific_ticker_last(self.ticker)
            #last = self.futureAPI.get_specific_ticker_last_pycurl(self.ticker)
            if last is not None:
                last = float(last)
                if self.openOrder is True:
                    self.checkPosition(last)

                if self.createOrder(last):
                    curTime = datetime.datetime.now()
                    self.checkWaiting(last, curTime)
                    self.mainDic[str(curTime)[11:19]] = last
                    print(str(curTime) + " " + str(last))
                self.lastPrice = last

    def checkPosition(self, last):
        try:
            result = self.futureAPI.get_specific_position(self.ticker)
            if result is not None and result['result']:
                dic = result['holding'][0]
                longAmount = int(dic['long_avail_qty'])
                if longAmount > 0:
                    if self.lastPrice > last:# or self.lastPrice == last:
                        self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687",
                                                  self.ticker, "3",
                                                  self.lastPrice, longAmount, 1, 20)
                        self.openOrder = False
                        self.message.emit("롱클로즈 바로 전가격 " + str(self.lastPrice) + "지금가격" + str(last))

                shortAmount = int(dic['short_avail_qty'])
                if shortAmount > 0:
                    if self.lastPrice < last:# or self.lastPrice == last:
                        self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687",
                                                  self.ticker, "4",
                                                  self.lastPrice, shortAmount, 1, 20)
                        self.openOrder = False
                        self.message.emit("숏클로즈 바로 전 가격 " + str(self.lastPrice) + "지금가격" + str(last))

        except (ConnectionError, Timeout, TooManyRedirects) as e:
            pass

    def createOrder(self, last):
        if self.sellWaiting is False and self.buyWaiting is False:
            return True

        if self.sellWaiting is True and self.lastPrice > last:
            self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687",
                                      self.ticker, "2",
                                      self.lastPrice, self.upAmount, 1, 20)
            self.sellWaiting = False
            self.openOrder = True
            self.message.emit("매도주문 시장가" + str(self.downAmount) + "개 현재가격" + str(self.lastPrice))
        if self.buyWaiting is True and self.lastPrice < last:
            self.futureAPI.take_order("f379a96206fa4b778e1554c6dc969687",
                                               self.ticker, "1",
                                               self.lastPrice, self.upAmount, 1, 20)
            self.buyWaiting = False
            self.openOrder = True
            self.message.emit("매수주문 시장가" + str(self.upAmount) + "개 현재가격" + str(self.lastPrice))

        return False

    def checkWaiting(self, last, curTime):
        if self.checkUp:
            checkTime = curTime - datetime.timedelta(0, self.upTime)
            key = str(checkTime)[11:19]
            if key in self.mainDic:
                checkPrice = self.mainDic[key]
                del(self.mainDic[key])
                if last > float(checkPrice) + (self.upTic * self.ticSize):
                    self.message.emit("매도 대기중 최근가격" + str(last) + "체크시간 + 틱가격" + str(float(checkPrice) + (self.upTic * self.ticSize)))
                    self.sellWaiting = True
                    
        if self.checkDown:
            checkTime = curTime - datetime.timedelta(0, self.downTime)
            key = str(checkTime)[11:19]
            if key in self.mainDic:
                checkPrice = self.mainDic[key]
                del (self.mainDic[key])
                if last < float(checkPrice) - (self.upTic * self.ticSize):
                    self.message.emit("매수 대기중 최근가격" + str(last) + "체크시간 - 틱가격" + str(float(checkPrice) + (self.upTic * self.ticSize)))
                    self.buyWaiting = True

        if self.mainDic.__len__() > 100:
            self.mainDic.clear()

    def setup_logger(self):
        # Prints logger info to terminal
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)  # Change this to DEBUG if you want a lot more info
        ch = logging.StreamHandler()
        # create formatter
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        # add formatter to ch
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        return logger
            


    def startThread(self, api_key, seceret_key, passphrase):
        self.api_key = api_key
        self.seceret_key = seceret_key
        self.passphrase = passphrase
        self.futureAPI = future.FutureAPI(api_key, seceret_key, passphrase, True)
        self._please_stop = False
        self.start()

    def stopThread(self):
        self._please_stop = True

    def setTicker(self, ticker):
        self.ticker = ticker

    def setUpTime(self, upTime):
        self.upTime = int(upTime)

    def setUpTic(self, upTic):
        self.upTic = int(upTic)

    def setUpAmount(self, upAmount):
        self.upAmount = int(upAmount)

    def setDownTime(self, downTime):
        self.downTime = int(downTime)

    def setDownTic(self, downTic):
        self.downTic = int(downTic)

    def setDownAmount(self, downAmount):
        self.downAmount = int(downAmount)

    def setTicSize(self, ticSize):
        self.ticSize = float(ticSize)

    def setChecked(self, startUp, stopDown, startDown, stopUp):
        if startUp or stopDown:
            self.checkUp = True
        if startDown or stopUp:
            self.checkDown = True
