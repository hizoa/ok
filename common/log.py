import sqlite3
from sqlite3 import Error
dbPath = "starbeta.sqlite"


def setDb(token, uuid, phrase):
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    sql = "DELETE from Info"
    cur.execute(sql)
    conn.commit()
    data = (token, uuid, phrase)
    sql = "INSERT  INTO  Info(api,secret,phrase) VALUES (?, ?, ?)"
    cur.execute(sql, data)
    conn.commit()
    conn.close()


def loadDb():
    conn = sqlite3.connect(dbPath)
    cur = conn.cursor()
    sql = "Create Table if not exists Info (api VARCHAR(30), secret VARCHAR(40), phrase VARCHAR(40))"
    cur.execute(sql)
    with conn:
        cur = conn.cursor()
        cur.execute("select * from Info")
        rows = cur.fetchall()
        for row in rows:
            print(row)
            dic = {"token": row[0], "uuid": row[1], "phrase": row[2]}
            return dic
    conn.commit()
    cur.close()
    conn.close()
    return None


def createCoinTable(coinList):
    try:
        conn = sqlite3.connect(dbPath)
        cur = conn.cursor()
        sql = "Create Table if not exists Coin (Coin VARCHAR(30) PRIMARY KEY, Reset VARCHAR(40), Amount VARCHAR(40), MinReverse VARCHAR(40), MaxReverse VARCHAR(40), Spread VARCHAR(40), Avg VARCHAR(40), LastPrice VARCHAR(40), Cut VARCHAR(40), CutPrice VARCHAR(40), Clear VARCHAR(40), Rest VARCHAR(40))"
        cur.execute(sql)
        for i, coin in enumerate(coinList):
            data = (coin['symbol'], "", "", "", "", "", "", "", "", "", "", "")
            sql = "INSERT  INTO  Coin(Coin,Reset,Amount,MinReverse,MaxReverse,Spread,Avg,LastPrice,Cut,CutPrice,Clear,Rest) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            cur.execute(sql, data)
        conn.commit()
        conn.close()
    except Error as e:
        print(str(e))


def saveCoinInfo(coin, reset, amount, min, max, spread, avg, last, cut, cutPrice, clear, rest):
    try:
        conn = sqlite3.connect(dbPath)
        cur = conn.cursor()

        data = (coin, reset, amount, min, max, spread, avg, last, cut, cutPrice, clear, rest)
        sql = "INSERT  OR REPLACE INTO  Coin(Coin,Reset,Amount,MinReverse,MaxReverse,Spread,Avg,LastPrice,Cut,CutPrice,Clear,Rest) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        cur.execute(sql, data)
        conn.commit()
        conn.close()
    except Error as e:
        print(str(e))


def loadCoinInfo(coinIndex):
    try:
        conn = sqlite3.connect(dbPath)
        with conn:
            cur = conn.cursor()
            sql = "Create Table if not exists Coin (Coin VARCHAR(30) PRIMARY KEY, Reset VARCHAR(40), Amount VARCHAR(40), MinReverse VARCHAR(40), MaxReverse VARCHAR(40), Spread VARCHAR(40), Avg VARCHAR(40), LastPrice VARCHAR(40), Cut VARCHAR(40), CutPrice VARCHAR(40), Clear VARCHAR(40), Rest VARCHAR(40))"
            cur.execute(sql)
            conn.commit()
            cur.execute("select * from Coin")
            rows = cur.fetchall()
            for row in rows:
                dic = {"coin": row[0], "reset": row[1], "amount": row[2], "min": row[3], "max": row[4], "spread": row[5], "avg": row[6], "last": row[7], "cut": row[8], "cutPrice": row[9], "clear": row[10], "rest": row[11]}
                if dic['coin'] == coinIndex:
                    return dic
    except Error as e:
        print(str(e))