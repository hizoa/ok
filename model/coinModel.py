from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtGui import QStandardItem
from PyQt5.QtCore import QVariant


class CoinModel(QStandardItemModel):
    def __init__(self, data=None, parent=None):
        QStandardItemModel.__init__(self, parent)
        for i, d in enumerate(data):
            col_1 = QStandardItem(d["instrument_id"])
            col_2 = QStandardItem(d["underlying_index"])
            self.setItem(i, 0, col_1)
            self.setItem(i, 1, col_2)
            self.setHorizontalHeaderLabels(["Instrument", "Index"])

    def data(self, QModelIndex, role=None):
        data = self.itemData(QModelIndex)
        if role == Qt.DisplayRole:
            return data[0]
        if role == Qt.UserRole:
            return data
        return QVariant()
